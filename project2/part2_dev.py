"""Scientific Computation Project 2, part 2
Your CID here: 01358568
"""
import numpy as np
import networkx as nx
from scipy.linalg import expm
from scipy.sparse.linalg import eigs
from scipy.integrate import odeint
import matplotlib.pyplot as plt


def rwgraph(G, i0=0, M=100, Nt=100):
    """ Question 2.1
    Simulate M Nt-step random walks on input graph, G, with all
    walkers starting at node i0
    Input:
        G: An undirected, unweighted NetworkX graph
        i0: intial node for all walks
        M: Number of walks
        Nt: Number of steps per walk
    Output: X: M x Nt+1 array containing the simulated trajectories
    """
    X = np.zeros((M, Nt + 1), dtype=int)
    A = nx.adjacency_matrix(G).todense()

    # initialising
    X[:, 0] = i0

    for j in range(M):
        for i in range(1, Nt + 1):
            X[j, i] = np.random.choice(np.nonzero(A[X[j, i - 1], :])[1], 1)[0]

    return X






def rwgraph_analyze1(input=(None)):
    """Analyze simulated random walks on
    Barabasi-Albert graphs.
    Modify input and output as needed.
    """
    ba1 = nx.barabasi_albert_graph(n=2000, m=4, seed=10)
    M = 10000
    Nt = 1000
    i0 = 7

    x = rwgraph_eff(ba1, i0, M, Nt)

    Xmax = np.max(np.abs(x[:, -1]))
    plt.hist(x[:, -1], bins=int(Xmax))
    plt.xlim(0, Xmax)
    plt.xlabel('position')
    plt.ylabel('number of walkers')
    plt.title('Distribution of Walks Across BA(2000,4)')

    M = 1000
    Nt = 10000
    i0 = 7

    x = rwgraph_eff(ba1, i0, M, Nt)

    Xmax = np.max(np.abs(x[:, -1]))
    plt.hist(x[:, -1], bins=int(Xmax))
    plt.xlim(0, Xmax)
    plt.xlabel('position')
    plt.ylabel('number of walkers')
    plt.title('Distribution of Walks Across BA(2000,4)')

    return None


def rwgraph_analyze2(input=(None)):
    """Analyze similarities and differences
    between simulated random walks and linear diffusion on
    Barabasi-Albert graphs.
    Modify input and output as needed.
    """

    Q = np.diag([ba1.degree(i) for i in range(ba1.number_of_nodes())])
    A = nx.adjacency_matrix(ba1).todense()
    I = np.eye(len(A))
    Qinv = np.linalg.inv(Q)

    L = Q - A
    Ls = I - Qinv * A
    Ls_trans = Ls.transpose()

    e, v = np.linalg.eig(L)

    L_sol = np.diag(e)



    return None #modify as needed


def modelA(G, x=0, i0=0.1, beta=1.0, gamma=1.0, tf=5, Nt=1000):
    """
    Question 2.2
    Simulate model A

    Input:
    G: Networkx graph
    x: node which is initially infected with i_x=i0
    i0: magnitude of initial condition
    beta,gamma: model parameters
    tf,Nt: Solutions are computed at Nt time steps from t=0 to t=tf (see code below)

    Output:
    iarray: N x Nt+1 Array containing i across network nodes at
                each time step.
    """

    N = G.number_of_nodes()
    iarray = np.zeros((N, Nt + 1))
    tarray = np.linspace(0, tf, Nt + 1)

    A = nx.adjacency_matrix(G).toarray()

    iarray[x, 0] = i0

    y0 = iarray[:, 0]

    def RHS(y, t):
        """Compute RHS of modelA at time t
        input: y should be a size N array
        output: dy, also a size N array corresponding to dy/dt

        Discussion: add discussion here
        """

        return [-beta * y[j] + gamma * (1 - y[j]) * np.dot(A[j, :], y) for j in range(N)]

    iarray = np.transpose(odeint(RHS, y0, tarray))

    return iarray


def modelB(G, x=0, i0=0.1, alpha=1.0, tf=5, Nt=1000):
    N = G.number_of_nodes()

    iarray = np.zeros((2 * N, Nt + 1))

    tarray = np.linspace(0, tf, Nt + 1)

    A = nx.adjacency_matrix(G).toarray()

    Q = np.diag([G.degree(i) for i in range(G.number_of_nodes())])

    L = Q - A

    iarray[x, 0] = i0

    y0 = iarray[:, 0]

    def RHS(y, t):
        dy = np.zeros((2 * N))
        dy[N:2 * N] = y[0:N]
        for j in range(0, N):
            dy[j] = alpha * (np.dot(L[j, :], y[N:2 * N]))

        return dy

    iarray = np.transpose(odeint(RHS, y0, tarray))

    return iarray


def lineardiff(G, x=0, i0=0.1, tf=5, Nt=1000):
    N = G.number_of_nodes()

    iarray = np.zeros((N, Nt + 1))

    tarray = np.linspace(0, tf, Nt + 1)

    A = nx.adjacency_matrix(G).toarray()

    Q = np.diag([G.degree(i) for i in range(G.number_of_nodes())])

    L = Q - A

    iarray[x, 0] = i0

    y0 = iarray[:, 0]

    e, v = np.linalg.eig(L)

    L_sol = np.diag(e)

    for j in range(1, Nt + 1):
        iarray[:, j] = np.dot(expm(tarray[j] * L_sol), y0)

    return iarray


def transport(input=(None)):
    """Analyze transport processes (model A, model B, linear diffusion)
    on Barabasi-Albert graphs.
    Modify input and output as needed.
    """
    ba2 = nx.barabasi_albert_graph(n=100, m=5, seed=10)

    A = modelA(ba2, beta=0.5, gamma=0.1, x=6)
    plt.plot(A[:, -1])
    B = modelB(ba2, alpha=-0.01, x=6)[0:100, :]
    plt.plot(B[:, -1])
    # L = lineardiff(ba2,x=6)
    # plt.plot(L[:,-1])
    plt.xlabel('position')
    plt.ylabel('Walker number')
    plt.title('Distribution of Walks Across BA(100,5)')
    plt.legend(['ModelA', 'ModelB'])
    plt.show()

    A = modelA(ba2, beta=0.5, gamma=0.1, x=50)
    plt.plot(A[:, -1])
    B = modelB(ba2, alpha=-0.01, x=50)[0:100, :]
    plt.plot(B[:, -1])
    plt.xlabel('position')
    plt.ylabel('Walker number')
    plt.title('Perturbed Distribution of Walks Across BA(100,5)')
    plt.legend(['ModelA', 'ModelB'])

    return None







if __name__=='__main__':
    rwgraph_analyze1
    transport()
    G=None #modify as needed
