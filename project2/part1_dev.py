"""Scientific Computation Project 2, part 1
Your CID here: 01358568
"""
from collections import deque

def flightLegs(Alist, start, dest):
    """
    Question 1.1
    Find the minimum number of flights required to travel between start and dest,
    and  determine the number of distinct routes between start and dest which
    require this minimum number of flights.
    Input:
        Alist: Adjacency list for airport network
        start, dest: starting and destination airports for journey.
        Airports are numbered from 0 to N-1 (inclusive) where N = len(Alist)
    Output:
        Flights: 2-element list containing:
        [the min number of flights between start and dest, the number of distinct
        jouneys with this min number]
        Return an empty list if no journey exist which connect start and dest
    """

    shortest = [float('infinity')] * len(Alist)
    count = [0] * len(Alist)

    shortest[start] = 0
    count[start] = 1

    Q = deque([start])

    while Q:
        u = Q.popleft()
        for v in Alist[u]:
            if not count[v]:
                Q.append(v)

            if shortest[u] + 1 <= shortest[v]:
                shortest[v] = shortest[u] + 1
                count[v] += count[u]

    Flights = [shortest[dest], count[dest]]

    if Flights == [float('inf'), 0]:
        Flights = []
    return Flights


def safeJourney(G, start, dest):
    """
    Question 1.2 i)
    Find safest journey from station start to dest
    Input:
        Alist: List whose ith element contains list of 2-element tuples. The first element
        of the tuple is a station with a direct connection to station i, and the second element is
        the density for the connection.
    start, dest: starting and destination stations for journey.

    Output:
        Slist: Two element list containing safest journey and safety factor for safest journey
    """

    # Initialize dictionaries
    Edict = {}  # Explored nodes
    Udict = {vertex: float('infinity') for vertex in range(len(G))}
    prev = {vertex: None for vertex in range(len(G))}

    # initialising the start node
    Udict[start] = 0

    # Main search
    while len(Udict) > 0:
        # Find node with min d in Udict and move to Edict
        dmin = float('infinity')
        for n, w in Udict.items():
            if w < dmin:
                dmin = w
                nmin = n
        Edict[nmin] = Udict.pop(nmin)

        # Update provisional distances for unexplored neighbors of nmin
        for n, w in G[nmin]:

            if n in Edict:
                pass
            elif n in Udict:
                dcomp = max(dmin, w)
                if dcomp <= Udict[n]:
                    Udict[n] = dcomp
                    prev[n] = nmin

    s, u = deque(), dest
    while prev[u]:
        s.appendleft(u)
        u = prev[u]
    s.appendleft(u)
    s.appendleft(start)
    return [list(s), Edict[dest]]

    return Slist


def shortJourney(G, start, dest):
    """
    Question 1.2 ii)
    Find shortest journey from station start to dest. If multiple shortest journeys
    exist, select journey which goes through the smallest number of stations.
    Input:
        Alist: List whose ith element contains list of 2-element tuples. The first element
        of the tuple is a station with a direct connection to station i, and the second element is
        the time for the connection (rounded to the nearest minute).
    start, dest: starting and destination stations for journey.

    Output:
        Slist: Two element list containing shortest journey and duration of shortest journey
    """

    Slist = [[], None]

    # Initialize dictionaries
    Edict = {}  # Explored nodes
    Udict = {vertex: float('infinity') for vertex in range(len(G))}
    dist_edges = {vertex: 0 for vertex in range(len(G))}
    prev = {vertex: None for vertex in range(len(G))}

    # initialising the start node
    Udict[start] = 0

    # Main search
    while len(Udict) > 0:
        # Find node with min d in Udict and move to Edict
        dmin = float('infinity')
        for n, w in Udict.items():
            if w < dmin:
                dmin = w
                nmin = n
        Edict[nmin] = Udict.pop(nmin)

        # Update provisional distances for unexplored neighbors of nmin
        for n, w in G[nmin]:

            if n in Edict:
                pass
            elif n in Udict:
                dcomp = dmin + w
                alt_edges = dist_edges[nmin] + 1
                if dcomp == Udict[n] and alt_edges < dist_edges[n]:
                    Udict[n] = dcomp
                    prev[n] = nmin
                    dist_edges[n] = alt_edges
                if dcomp < Udict[n]:
                    Udict[n] = dcomp;
                    dist_edges[n] = alt_edges;
                    prev[n] = nmin;
            else:
                dcomp = dmin + w
                Udict[n] = dcomp

    s, u = deque(), dest
    while prev[u]:
        s.appendleft(u)
        u = prev[u]
    s.appendleft(u)
    s.appendleft(start)
    Slist = [list(s), Edict[dest]]

    return Slist


def cheapCycling(SList, CList):
    """
    Question 1.3
    Find first and last stations for cheapest cycling trip
    Input:
        Slist: list whose ith element contains cheapest fare for arrival at and
        return from the ith station (stored in a 2-element list or tuple)
        Clist: list whose ith element contains a list of stations which can be
        cycled to directly from station i
    Stations are numbered from 0 to N-1 with N = len(Slist) = len(Clist)
    Output:
        stations: two-element list containing first and last stations of journey
    """

    # compute an ordered list of cheapest arrival + return journeys in tuple format (sum,i,j)
    # for each item in the list, compute bfs from start, if return is reachable, end, otherwise keep on going

    # compute a list of all possible arrival + return journeys to make in tuple format (sum,arrival_price,return_price)
    l = [(SList[i][0] + SList[j][1], i, j) for i in range(len(SList)) for j in range(len(SList))]

    # sort it, so that the cheapest option is first
    l = sorted(l, key=lambda x: x[0])

    # pick the first item in the ordered list. If journey from start to end is reachable by bfs, output those stations. Otherwise, keep going
    for k in range(len(l)):
        s = l[k][1]
        d = l[k][2]
        if bfs(CList, s)[d] == 1:
            stations = [s, d]
            break

    return stations


if __name__ == '__main__':
    # add code here if/as desired
    L = None  # modify as needed
