"""Scientific Computation Project 3, part 1
CID: 01358568
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from time import time
from scipy import sparse
from scipy.special import hankel1, hankel2
from scipy.linalg import solve_banded
from scipy.signal import welch
from scipy import stats

def hfield(r,th,h,levels=50):
    """Displays height field stored in 2D array, h,
    using polar grid data stored in 1D arrays r and th.
    Modify as needed.
    """
    thg,rg = np.meshgrid(th,r)
    xg = rg*np.cos(thg)
    yg = rg*np.sin(thg)
    plt.figure()
    plt.contourf(xg,yg,h,levels)
    plt.axis('equal')
    return None

def repair1(R,p,l=1.0,niter=10,inputs=()):
    """
    Question 1.1: Repair corrupted data stored in input
    array, R.
    Input:
        R: 2-D data array (should be loaded from data1.npy)
        p: dimension parameter
        l: l2-regularization parameter
        niter: maximum number of iterations during optimization
        inputs: can be used to provide other input as needed
    Output:
        A,B: a x p and p x b numpy arrays set during optimization
    """
    #problem setup
    R0 = R.copy()
    a,b = R.shape
    iK,jK = np.where(R0 != -1000) #indices for valid data
    aK,bK = np.where(R0 == -1000) #indices for missing data

    S = set()
    for i,j in zip(iK,jK):
            S.add((i,j))

    #Set initial A,B
    A = np.ones((a,p))
    B = np.ones((p,b))

    #Create lists of indices used during optimization
    mlist = [[] for i in range(a)]
    nlist = [[] for j in range(b)]

    for i,j in zip(iK,jK):
        mlist[i].append(j)
        nlist[j].append(i)

    dA = np.zeros(niter)
    dB = np.zeros(niter)

    for k in range(niter):
        Aold = A.copy()
        Bold = B.copy()

        #Loop through elements of A and B in different
        #order each optimization step
        for m in np.random.permutation(a):
            for n in np.random.permutation(b):
                if n < p: #Update A[m,n]
                    Bfac = 0.0
                    Asum = 0

                    for j in mlist[m]:
                        Bfac += B[n,j]**2
                        Rsum = 0
                        for k in range(p):
                            if k != n: Rsum += A[m,k]*B[k,j]
                        Asum += (R[m,j] - Rsum)*B[n,j]

                    A[m,n] = Asum/(Bfac+l) #New A[m,n]
                if m<p:
                    #Add code here to update B[m,n]
                    B[m,n]=None #modify
        dA[k] = np.sum(np.abs(A-Aold))
        dB[k] = np.sum(np.abs(B-Bold))
        if k%10==0: print("k,dA,dB=",k,dA[k],dB[k])


    return A,B


def repair2(R,p,l=1.0,niter=10,inputs=()):
    """
    Question 1.1: Repair corrupted data stored in input
    array, R.
    Input:
        R: 2-D data array (should be loaded from data1.npy)
        p: dimension parameter
        l: l2-regularization parameter
        niter: maximum number of iterations during optimization
        inputs: can be used to provide other input as needed
    Output:
        A,B: a x p and p x b numpy arrays set during optimization
    """
    #problem setup
    R0 = R.copy()
    a,b = R.shape
    iK,jK = np.where(R0 != -1000) #indices for valid data
    aK,bK = np.where(R0 == -1000) #indices for missing data

    S = set()
    for i,j in zip(iK,jK):
            S.add((i,j))

    #Set initial A,B
    A = np.ones((a,p))
    B = np.ones((p,b))

    #Create lists of indices used during optimization
    mlist = [[] for i in range(a)]
    nlist = [[] for j in range(b)]

    for i,j in zip(iK,jK):
        mlist[i].append(j)
        nlist[j].append(i)

    dA = np.zeros(niter)
    dB = np.zeros(niter)

    for k in range(niter):
        Aold = A.copy()
        Bold = B.copy()

        #Loop through elements of A and B in different
        #order each optimization step
        for m in np.random.permutation(a):
            for n in np.random.permutation(b):
                if n < p: #Update A[m,n]
                    Bfac = sum([B[n,j]**2 for j in mlist[m]])
                    Asum = sum([(R[m,j] - sum([A[m,k]*B[k,j] for k in range(p)]) - A[m,n]*B[n,j])*B[n,j] for j in mlist[m]])
                    A[m,n] = Asum/(Bfac+l) #New A[m,n]

                if m < p: #Update B[m,n]
                    Afac = sum([A[i,m]**2 for i in mlist[m]])
                    Bsum = sum([(R[i,n] - sum([A[i,k]*B[k,n] for k in range(p)]) - A[i,m]*B[m,n])*A[i,m] for i in mlist[m]])
                    B[m,n]= Bsum/(Afac+l) #New B[m,n]

        #do i need this?
        dA[k] = np.sum(np.abs(A-Aold))
        dB[k] = np.sum(np.abs(B-Bold))
        if k%10==0: print("k,dA,dB=",k,dA[k],dB[k])

    return A,B


def outwave(r0):
    """
    Question 1.2i)
    Calculate outgoing wave solution at r=r0
    See code/comments below for futher details
        Input: r0, location at which to compute solution
        Output: B, wave equation solution at r=r0

    """

    A = np.load('data2.npy')

    # range of r from 1 to 5
    r = np.load('r.npy')
    # range of theta from 0 to 2*pi
    th = np.load('theta.npy')

    Nr, Ntheta, Nt = A.shape
    B = np.zeros((Ntheta, Nt))

    # initial conditions of the height in a circle of radius 1 for all t and theta
    f = A[0, :, :]

    def R(k, r, n):
        return hankel1(n, k * r)

    def T(k, t):
        return np.exp(-1j * k * t)

    def THETA(n, theta_var):
        return np.exp(1j * n * theta_var)

    def soln(k, n, theta_var, r, t):
        return R(k, r, n) * T(k, t) * THETA(n, theta_var)

    c = np.fft.rfft(f[:, 0]) / 289
    k = 20

    for th in range(len(theta)):
        for t in range(20):
            B[th, t] = sum([c[i] * soln(k, i, theta[th], r0, t) for i in range(12)])

    return B

def analyze1():
    """
    Question 1.2ii)
    Add input/output as needed

    """

    return None #modify as needed




def reduce(H,inputs=()):
    """
    Question 1.3: Construct one or more arrays from H
    that can be used by reconstruct
    Input:
        H: 3-D data array
        inputs: can be used to provide other input as needed
    Output:
        arrays: a tuple containing the arrays produced from H
    """

    #Add code here

    arrays = () #modify as needed
    return arrays


def reconstruct(arrays,inputs=()):
    """
    Question 1.3: Generate matrix with same shape as H (see reduce above)
    that has some meaningful correspondence to H
    Input:
        arrays: tuple generated by reduce
        inputs: can be used to provide other input as needed
    Output:
        Hnew: a numpy array with the same shape as H
    """

    #Add code here

    Hnew = None #modify

    return Hnew


if __name__=='__main__':
    x=None
    #Add code here to call functions above and
    #generate figures you are submitting
