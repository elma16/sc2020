""" Your college id here: 01358568
    Template code for part 2, contains 3 functions:
    codonToAA: returns amino acid corresponding to input amino acid
    DNAtoAA: to be completed for part 2.1
    pairSearch: to be completed for part 2.2
"""


def codonToAA(codon):
	"""Return amino acid corresponding to input codon.
	Assumes valid codon has been provided as input
	"_" is returned for valid codons that do not
	correspond to amino acids.
	"""
	table = {
		'ATA':'i', 'ATC':'i', 'ATT':'i', 'ATG':'m',
		'ACA':'t', 'ACC':'t', 'ACG':'t', 'ACT':'t',
		'AAC':'n', 'AAT':'n', 'AAA':'k', 'AAG':'k',
		'AGC':'s', 'AGT':'s', 'AGA':'r', 'AGG':'r',
		'CTA':'l', 'CTC':'l', 'CTG':'l', 'CTT':'l',
		'CCA':'p', 'CCC':'p', 'CCG':'p', 'CCT':'p',
		'CAC':'h', 'CAT':'h', 'CAA':'q', 'CAG':'q',
		'CGA':'r', 'CGC':'r', 'CGG':'r', 'CGT':'r',
		'GTA':'v', 'GTC':'v', 'GTG':'v', 'GTT':'v',
		'GCA':'a', 'GCC':'a', 'GCG':'a', 'GCT':'a',
		'GAC':'d', 'GAT':'d', 'GAA':'e', 'GAG':'e',
		'GGA':'g', 'GGC':'g', 'GGG':'g', 'GGT':'g',
		'TCA':'s', 'TCC':'s', 'TCG':'s', 'TCT':'s',
		'TTC':'f', 'TTT':'f', 'TTA':'l', 'TTG':'l',
		'TAC':'y', 'TAT':'y', 'TAA':'_', 'TAG':'_',
		'TGC':'c', 'TGT':'c', 'TGA':'_', 'TGG':'w',
	}
	return table[codon]


def DNAtoAA(S):
    """Convert genetic sequence contained in input string, S,
    into string of amino acids corresponding to the distinct
    amino acids found in S and listed in the order that
    they appear in S
    """
    AA = [codonToAA(S[0:3])]
    for i in range(1,len(S)//3):
        codon = codonToAA(S[3*i:3*(i+1)])
        if AA[len(AA)-1]!=codon:
            AA.append(codon)
    AA = ''.join(AA)
    return AA


def RK(text, pattern, q):
    n = len(text)
    m = len(pattern)
    h = pow(4,m-1,q)
    p = 0
    t = 0
    result = []
    for i in range(m):
        p = (4*p+ord(pattern[i]))% q
        t = (4*t+ord(text[i]))% q
    for s in range(n-m+1):
        if p == t:
            match = True
            for i in range(m):
                if pattern[i] != text[s+i]:
                    match = False
                    break
            if match:
                result = result + [s]
        if s < n-m:
            t = (t-h*ord(text[s]))%q
            t = (t*4+ord(text[s+m]))%q
            t = (t+q)%q
    return result

def pairSearch(L,pairs):
    """Find locations within adjacent strings (contained in input list,L)
    that match k-mer pairs found in input list pairs. Each element of pairs
    is a 2-element tuple containing k-mer strings
    """
    locations = []
    for string1 in L:
        for string2 in L:
            for pair in pairs:
                if RK(string1,pair[0],19) == RK(string2,pair[1],19) and RK(string1,pair[0],19) != []:
                    locations.append([RK(string1,pair[0],19)[0],L.index(string1),pairs.index(pair)])
    return locations


def naive_search(S, P):
	imatch = []
	N = len(S)
	# Set pattern for search
	M = len(P)
	for ind in range(0, N - M + 1):
		matching = True
		# Compare sub-string to pattern
		for count, indp in enumerate(range(ind, ind + M)):
			if P[count] != S[indp]:
				matching = False
				break

		# Update list when match found
		if matching:
			imatch.append(ind)
			print("Match found, i=", ind)
