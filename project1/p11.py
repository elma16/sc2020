""" Your college id here: 01358568
    Template code for part 1, contains 4 functions:
    newSort, merge: codes for part 1.1
    time_newSort: to be completed for part 1.1
    findTrough: to be completed for part 1.2
"""
import numpy as np
import matplotlib.pyplot as plt

%matplotlib inline



def newSort(X,k=0):
    """Given an unsorted list of integers, X,
        sort list and return sorted list
    """
    #all comments added thereafter are my own

    #compute the length of the list
    n = len(X)
    #if the length of the list is one, it is already sorted
    if n==1:
        return X
    #if the length of the list is less than this k look at each element of hte list in turn, call it the smallest elt
    #if an element of the list is smaller than it, swap the two places of the elts, call it the smallest, repeat
    elif n<=k:
        for i in range(n-1):
            ind_min = i
            for j in range(i+1,n):
                if X[j]<X[ind_min]:
                    ind_min = j
            X[i],X[ind_min] = X[ind_min],X[i]
        return X
    #if it's bigger than k, split it until you can perform this alg on it, then merge the sorted lists at the end

    else:
        L = newSort(X[:n//2],k)
        R = newSort(X[n//2:],k)
        return merge(L,R)


def merge(L,R):
    """Merge 2 sorted lists provided as input
    into a single sorted list
    """
    M = [] #Merged list, initially empty
    indL,indR = 0,0 #start indices
    nL,nR = len(L),len(R)

    #Add one element to M per iteration until an entire sublist
    #has been added
    for i in range(nL+nR):
        if L[indL]<R[indR]:
            M.append(L[indL])
            indL = indL + 1
            if indL>=nL:
                M.extend(R[indR:])
                break
        else:
            M.append(R[indR])
            indR = indR + 1
            if indR>=nR:
                M.extend(L[indL:])
                break
    return M


def time_newSort(inputs=None):
    """Analyze performance of newSort
    Use variables inputs and outputs if/as needed
    """
    random = []
    worst = []
    best = []

    # average case scenario input
    for k in range(10):
        for i in range(6):
            N = 10 ** i
            X = list(np.random.randint(1, 2 * N, N))
            t1 = time.time()
            y = newSort(X, k)
            t2 = time.time()
            random.append((N, t2 - t1))

    # worst case scenario input
    for k in range(10):
        for i in range(6):
            N = 10 ** i
            X = list(range(N, 0, -1))
            t1 = time.time()
            y = newSort(X, k)
            t2 = time.time()
            worst.append((N, t2 - t1))

    # best case scenario input
    for k in range(10):
        for i in range(6):
            N = 10 ** i
            X = list(range(N))
            t1 = time.time()
            y = newSort(X, k)
            t2 = time.time()
            best.append((N, t2 - t1))

    fig, ax = plt.subplots()
    fig.set_size_inches(18.5, 10.5)
    ax.set_yscale('log')
    for i in range(len(worst)):
        w = worst[6 * i:6 * (i + 1)]
        ax.plot(*zip(*w), label='{i}'.format(i=i))
    ax.set_xlabel('N')
    ax.set_ylabel('Time')
    plt.title('Plot of size of list against time, for $n \leq k$, $k$ varied: Worst Case')
    plt.legend(loc='best')

    fig, ax = plt.subplots()
    fig.set_size_inches(18.5, 10.5)
    ax.set_yscale('log')
    for i in range(len(random)):
        r = random[6 * i:6 * (i + 1)]
        ax.plot(*zip(*r), label='{i}'.format(i=i))
    ax.set_xlabel('N')
    ax.set_ylabel('Time')
    plt.title('Plot of size of list against time, for $n \leq k$, $k$ varied: Average Case')
    plt.legend(loc='best')

    fig, ax = plt.subplots()
    fig.set_size_inches(18.5, 10.5)
    ax.set_yscale('log')
    for i in range(len(best)):
        b = best[6 * i:6 * (i + 1)]
        ax.plot(*zip(*b), label='{i}'.format(i=i))
    ax.set_xlabel('N')
    ax.set_ylabel('Time')
    plt.title('Plot of size of list against time, for $n \leq k$, $k$ varied: Best Case')
    plt.legend(loc='best')

    random2 = []
    worst2 = []
    best2 = []

    # average case scenario input
    for k in range(10 ** 5, 10 ** 5 + 10):
        for i in range(4):
            N = 10 ** i
            X = list(np.random.randint(1, 2 * N, N))
            t1 = time.time()
            y = newSort(X, k)
            t2 = time.time()
            random2.append((N, t2 - t1))

    # worst case scenario input
    for k in range(10 ** 5, 10 ** 5 + 10):
        for i in range(4):
            N = 10 ** i
            X = list(range(N, 0, -1))
            t1 = time.time()
            y = newSort(X, k)
            t2 = time.time()
            worst2.append((N, t2 - t1))

    # best case scenario input
    for k in range(10 ** 5, 10 ** 5 + 10):
        for i in range(4):
            N = 10 ** i
            X = list(range(N))
            t1 = time.time()
            y = newSort(X, k)
            t2 = time.time()
            best2.append((N, t2 - t1))

    fig, ax = plt.subplots()
    fig.set_size_inches(18.5, 10.5)
    ax.set_yscale('log')
    for i in range(len(worst2)):
        w2 = worst2[4 * i:4 * (i + 1)]
        ax.plot(*zip(*w2), label='{i}'.format(i=10 ** 5 + i))
    ax.set_xlabel('N')
    ax.set_ylabel('Time')
    plt.title('Plot of size of list against time, for $n > k$, $k$ varied: Worst Case')
    plt.legend(loc='best')

    fig, ax = plt.subplots()
    fig.set_size_inches(18.5, 10.5)
    ax.set_yscale('log')
    for i in range(len(random2)):
        r2 = random2[4 * i:4 * (i + 1)]
        ax.plot(*zip(*r2), label='{i}'.format(i=10 ** 5 + i))
    ax.set_xlabel('N')
    ax.set_ylabel('Time')
    plt.title('Plot of size of list against time, for $n > k$, $k$ varied: Average Case')
    plt.legend(loc='best')

    fig, ax = plt.subplots()
    fig.set_size_inches(18.5, 10.5)
    ax.set_yscale('log')
    for i in range(len(best2)):
        b2 = best2[4 * i:4 * (i + 1)]
        ax.plot(*zip(*b2), label='{i}'.format(i=10 ** 5 + i))
    ax.set_xlabel('N')
    ax.set_ylabel('Time')
    plt.title('Plot of size of list against time, for $n > k$, $k$ varied: Best Case')
    plt.legend(loc='best')
    outputs=None
    return outputs

def findTrough(L):
    """Find and return a location of a trough in L
    """
    if L[0] <= L[1]:
        return 0
    elif L[len(L)-1] <= L[len(L)-2]:
        return len(L)-1
    else:
        for j in range(1,len(L)):
            if L[j-1] >= L[j] <= L[j+1]:
                return j
        else:
            return -(len(L)+1)


if __name__=='__main__':
    inputs=None
    outputs=time_newSort(inputs)
